// Display Dependencies
#include <Wire.h>
#include <Adafruit_LEDBackpack.h>
#include <Adafruit_GFX.h>

Adafruit_AlphaNum4 anzeige = Adafruit_AlphaNum4();

// Network Dependencies
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

/* 
 *  Variablen fuer die Netzwerkkommunikation
 *  setze hier die IP Adressen abhaengig von deinen Netzwerkeinstellungen ein.
 */
const char* ssid = "YourWiFiSSID";
const char* password = "yourWiFiPassword";

IPAddress ip(192,168,178,110); // Die IP Adresse des Numpad
IPAddress gateway(192,168,178,1); // Die IP Adresse des Routers
IPAddress subnet(255,255,255,0); // Die Subnetzmaske. Fuer gewoehnlich (255,255,255,0)
IPAddress outIP (192,168,178,51); // Die IP Adresse der Zentralen Schnittstellen Software

unsigned int localPort = 8888; // Der Kommunikationsport
WiFiUDP UDP;

// Die JSON Library einbinden
#include <ArduinoJson.h>

StaticJsonBuffer<200> jsonBuffer;
JsonObject& sendData = jsonBuffer.createObject();

char device_name[10] = "zeitbombe";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println(device_name);

  if(connectWifi()) {
    if(connectUDP()) {
      sendData["status"] = "connected";
      sendUDPstr(sendData, outIP);
    }
  }

  anzeige.begin(0x70);

  for (int i = 0; i < 4; i++)
  {
    // LED anzeige testen
    anzeige.clear();
    anzeige.writeDigitRaw(i, 0x3FFF);
    anzeige.writeDisplay();

    delay(200);
  }

  reset();
  
  // Infos auf LED Display anzeigen
  char starttext[125];

  strcpy(starttext, device_name); /* copy name into the new var */
  strcat(starttext, "     "); /* add the extension */;

  displayString(starttext, 400);
}

void loop() {
  receiveUDP();
  scrollDisplay();

  int timecapture = updateCountdown(false);

  if (timecapture == 0)
  {
    timeover();
  }
}

/*
 *  Den Countdown zuruecksetzen und starten
 */
void start(int minutes, int seconds)
{
  setPulse(1000);
  startCountdown(minutes, seconds);

  setScrolltext(false);

  // gameRunning = true;
  // gameLost = false;
  sendData["status"] = "counting";
  sendUDPstr(sendData, outIP);

  Serial.println("START");
}

void timeover()
{
  sendData["status"] = "timeover";
  sendUDPstr(sendData, outIP);
}

/*
 * Das Display und die LEDs deaktivieren und auf Starteingabe warten
 */
void reset()
{
  displayText("RST ");
  setScrolltext(false);
  // gameRunning = false;
  // gameWon = false;

  Serial.println("RESET");

  delay(1000);

  anzeige.clear();
  anzeige.writeDisplay();
}

void incomming(JsonObject& msg)
{
  if(msg["start"]) {
     start(msg["start"]["min"], msg["start"]["sec"]);
  } else if(msg["pause"]) {
     pauseCountdown();
     sendData["status"] = "paused";
     sendUDPstr(sendData, outIP);
  } else if(msg["resume"]) {
     resumeCountdown();
     sendData["status"] = "counting";
     sendUDPstr(sendData, outIP);
  sendUDPstr(sendData, outIP);
  } else if(msg["scroll"]) {
     displayString(msg["scroll"]["text"], msg["scroll"]["interval"]);
  } else if(msg["show"] == "counter") {
    countVisible(true);
  } else if(msg.containsKey("text")) {
    char buffer[5];
    String string;
    msg["text"].printTo(string);
    //sprintf(buffer, "%.4s", string);
    //Serial.println(buffer);
    string.toCharArray(buffer, 4);
    displayText(buffer);
  }
}

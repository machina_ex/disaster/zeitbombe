/*
* Netzwerk Funktionen
*
*
*/
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //Zwischenspeicher fuer eingehende Daten
char ReplyBuffer[] = "acknowledged"; // Bestaetigungsnachricht

/*
*  UDP Verbindung einrichten
*  Gibt true zurueck wenn erfolgreich
*/
bool connectUDP()
{
    bool state = false;
    
    Serial.println("");
    Serial.println("Connecting to UDP");
    
    if(UDP.begin(localPort) == 1)
    {
      Serial.println("Connection successful");
      state = true;
    }
    else
    {
      Serial.println("Connection failed");
    }
    
    return state;
}

/*
* Mit W-lan verbinden
* gibt true zureuck wenn erfolgreich
*/
bool connectWifi()
{
    boolean state = true;
    int i = 0;
    WiFi.config(ip,gateway,subnet);// optional if we want to get an IP of our choice if possible
    WiFi.begin(ssid, password);
    Serial.println("");
    Serial.println("Connecting to WiFi");
    
    // Wait for connection
    Serial.print("Connecting");
    while (WiFi.status() != WL_CONNECTED) 
    {
      delay(500);
      Serial.print(".");
      if (i > 10){
      state = false;
      break;
      }
      i++;
    }
    if (state)
    {
      Serial.println("");
      Serial.print("Connected to ");
      Serial.println(ssid);
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
      
      char myIp[24];
      sprintf(myIp, "%d.%d.%d.%d",ip[0],ip[1],ip[2],ip[3]);
      
          // send hello I'm there to the home base:
          char hello[48];
          sprintf(hello, "15kGy_Numpad online as:%s:%d", myIp,localPort);
          //sendUDPstr(hello, outIP);

    }
    else 
    {
      Serial.println("");
      Serial.println("Connection failed.");
    }
    return state;
}


/*
*  UDP Nachricht versenden
*/
void sendUDPstr(JsonObject& message, IPAddress outIPInput)
{
  char char_msg[200];
  message.printTo(char_msg, 200);
  Serial.print("send: ");
  Serial.println(char_msg);
  UDP.beginPacket(outIPInput, localPort);
  UDP.write(char_msg);
  UDP.endPacket();
}

void receiveUDP()
{
  // if there’s data available, read a packet
  int packetSize = UDP.parsePacket();
  if(packetSize)
  {
    // read the packet into packetBufffer
    UDP.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    Serial.print("incomming: ");
    Serial.println(packetBuffer);
    toJSON(packetBuffer); //forward UDPinput into Serial
    for(int i=0;i<UDP_TX_PACKET_MAX_SIZE;i++) packetBuffer[i] = 0; //empty the buffer again
  }
}

void toJSON(String data) {
  StaticJsonBuffer<200> jsonInBuffer;
  JsonObject& receiveData = jsonInBuffer.parseObject(data);
  incomming(receiveData);
}

# zeitbombe

## Requirements

* ArduinoJSON library version 5. Version > 5 will not work without modifications!
* Adafruit_LEDBackpack library
* Adafruit_GFX library
* ESP8266WiFi library
* WiFiUdp library

## Setup

Change Wifi SSID, Password and IP Addresses to match your Network specifications.

``` c++
const char* ssid = "YourWiFiSSID";
const char* password = "yourWiFiPassword";

IPAddress ip(192,168,178,110); // IP Address of zeitbombe device
IPAddress gateway(192,168,178,1); // Router IP Address
IPAddress subnet(255,255,255,0); // Subnet Mask. Usually this defaults to (255,255,255,0)
IPAddress outIP (192,168,178,51); // IP Address of Interface software to connect to.
```
// Variablen fuer Countdown
boolean counting = false;
boolean count_visible = false;
int pulse = 1000; // Zeit jede Sekunde anpassen
unsigned long int timestamp = 0; // Wird bei jeder Aktualisierung des Displays angepasst

int seconds = 0;
int minutes = 0;

int skipcount = 0;

// Variablen fuer das Scrollen des Textes ueber das Display
boolean scrolltext = false;
char displaybuffer[4] = {' ', ' ', ' ', ' '};
String stringbuffer;
int scrollinterval;
int scrollcount = 0;

unsigned long int timecapture = 0;

/*
 * Stellt die Zeit im laufenden Betrieb um
 */
void setCount(int newminutes, int newseconds)
{
  minutes = newminutes;
  seconds = newseconds;
  updateCountdown(true);
}
/*
*  Startet einen Countdown beginnend bei angegebenem Zeitpunkt
*/
void startCountdown(int startminutes, int startseconds)
{
  counting = true;
  
  seconds = startseconds;
  minutes = startminutes;

  countVisible(true);
}

void countVisible(boolean visible)
{
  count_visible = visible;
  if(visible) {
    scrolltext = false;
    // Dezimalpunkt anstellen
    anzeige.writeDigitRaw(1, 0x4000);
    anzeige.writeDisplay();
    updateCountdown(true);
  }
  Serial.print("SHOW ");
  Serial.println(count_visible);
  
}

/*
*  setzt den Countdown um angegebene Zeit vor
*/
void skipTime(int amount)
{
  skipcount = amount;
  setPulse(10);
}

/*
* legt fest in welchem Takt der Countdown ablaueft
*/
void setPulse(int newpulse)
{
  pulse = newpulse;
}

// Muss im Loop aufgerufen werden
int updateCountdown(boolean setonce)
{
  if(counting) 
  {
    if (timestamp + pulse < millis() || setonce)
    {
      // For the skip Time Function
      if (skipcount > 0)
      {
        skipcount--;
      } else {
        setPulse(1000);
      }
      seconds--;
      // mm ss anzeige uebertrag
      if ( seconds >= 60 )
      {
        minutes++;
        seconds = 0;
      }
      else if ( seconds < 0 )
      {
        minutes--;
        seconds = 59;
      }
  
      int timevalue = minutes * 100 + seconds;
  
      // An Grenzbereichen timer halten aber nicht stoppen
      if ( timevalue < 0 )
      {
        timevalue = 0;
        minutes = 0;
        seconds = 0;
      }
      if ( timevalue > 9959 )
      {
        timevalue = 9995;
        minutes = 99;
        seconds = 59;
      }

      if(count_visible) {
        displayNumber(timevalue);
      }
      
      timestamp = millis();
  
      return (timevalue);
  
    } else {
      return (-1);
    }
  } else {
    return (-1);
  }
}

void pauseCountdown() {
  counting = false;
  Serial.println("PAUSE");
}

void resumeCountdown() {
  counting = true;
  scrolltext = false;
  Serial.println("RESUME");
}

void displayNumber(int number)
{
  char str[4];

  for (int i = 3; i >= 0; i--) {
    str[i] = number % 10 + '0';
    number /= 10;
  }

  displayText(str);
}

void displayText(char newText[4])
{
  scrolltext = false;
  anzeige.writeDigitAscii(0, newText[0]);
  anzeige.writeDigitAscii(1, newText[1], true);
  //anzeige.writeDigitRaw(1, 0x4000);
  anzeige.writeDigitAscii(2, newText[2]);
  anzeige.writeDigitAscii(3, newText[3]);
  anzeige.writeDisplay();
}

void displayString(String text, int interval)
{
  count_visible = false;
  scrolltext = true;
  scrollcount = 0;
  stringbuffer = text;
  scrollinterval = interval;
  timecapture = millis();

  displaybuffer[0] = ' ';
  displaybuffer[1] = ' ';
  displaybuffer[2] = ' ';
  displaybuffer[3] = ' ';
}

/*
 * Srollt den stringbuffer ueber das Display. Wird im Loop aufgerufen
 */
void scrollDisplay()
{ 
  if (timecapture + scrollinterval < millis() && scrolltext)
  {
    timecapture = millis();

    displaybuffer[0] = displaybuffer[1];
    displaybuffer[1] = displaybuffer[2];
    displaybuffer[2] = displaybuffer[3];
    displaybuffer[3] = stringbuffer[scrollcount];

    anzeige.writeDigitAscii(0, displaybuffer[0]);
    anzeige.writeDigitAscii(1, displaybuffer[1]);
    anzeige.writeDigitAscii(2, displaybuffer[2]);
    anzeige.writeDigitAscii(3, displaybuffer[3]);

    anzeige.writeDisplay();

    if (scrollcount < stringbuffer.length()-1 )
    {
      scrollcount++;
    } else {
      scrollcount = 0;
    }

  }
}
void setScrolltext(boolean scrolltextstatus)
{
  scrolltext = scrolltextstatus;
}

